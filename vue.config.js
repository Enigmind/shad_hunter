module.exports = {
  pages: {
    index: {
      entry: './src/vue/index.ts',
    },
  },
  pluginOptions: {
    electronBuilder: {
      mainProcessFile: './src/core/index.ts',
      preload: './src/vue/preload.ts',
    },
  },
}
