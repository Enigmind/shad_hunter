# SHAD Hunter

## Presentation

Desktop application to hunt best opportunities on SHAD strategy via Binance API.
The used techno are :
- Electron for code and windows execution
- TypeScript
- VueJS for front elements and views
- Vuetify
- fastest-validator for data validation
- momentJS for date management
- TypeORM to manage datas in the SQLite database

The following chart define how the flows works inside the app :

```mermaid
flowchart LR
  vue[Composant Vue] <--> ipc[Bus IPC];
  ipc <--> routes[Routes];
  routes <--> controller[Controller];
  controller <--> model[Model];
  model <--> typeORM[TypeORM];
  typeORM <--> sqlite[SQLite DB];
```

The dev environment has been tested ans is working on Linux and Windows. It may works on MacOS as well.

## Setup

``` bash
npm i
```

### Dev environment

``` bash
npm run dev
```

### Code Linting

``` bash
npm run lint
```

### Compilation and executable building

``` bash
npm run build
```

By default, the `build` command generate a Linux executable but you can easily create Windows and Mac working executable using the [official documentation](https://www.electron.build/).
