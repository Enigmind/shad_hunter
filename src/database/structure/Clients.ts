import { EntitySchema } from 'typeorm'

// eslint-disable-next-line import/no-unused-modules
export default new EntitySchema({
  name: 'Clients',
  columns: {
    id: {
      type: 'varchar',
      primary: true,
      generated: 'uuid',
    },
    firstName: {
      type: 'varchar',
    },
    lastName: {
      type: 'varchar',
    },
  },
})
