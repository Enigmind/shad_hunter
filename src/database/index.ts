import path from 'path'

import { Connection, createConnection } from 'typeorm'
// eslint-disable-next-line import/no-extraneous-dependencies
import { app } from 'electron'

import Clients from './structure/Clients'

let connection: Connection

// eslint-disable-next-line import/no-unused-modules
export async function connect(): Promise<void> {
  connection = await createConnection({
    type: 'sqlite',
    synchronize: true,
    database: path.join(app.getPath('userData'), '/database.sqlite'),
    entities: [Clients],
  })
}

// eslint-disable-next-line import/no-unused-modules
export async function disconnect(): Promise<void> {
  await connection.close()
}

// eslint-disable-next-line import/no-unused-modules
export function getConnection(): Connection {
  return connection
}
