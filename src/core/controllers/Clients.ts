import FastestValidator, { ValidationRule } from 'fastest-validator'

import Client from '../../entities/Client'
import Model from '../models/Clients'

import DefaultController from './DefaultController'

const fastestValidator = new FastestValidator({
  useNewCustomCheckerFunction: true,
})

const schemas = {
  create: {
    firstName: { type: 'string' },
    lastName: { type: 'string', min: 1 },
  },
} as {
  [key: string]: {
    [key: string]: ValidationRule,
  },
}

const validators = {
  create: fastestValidator.compile(schemas.create),
  update: fastestValidator.compile(schemas.create),
}

export default new DefaultController<Client>(Model, validators)
