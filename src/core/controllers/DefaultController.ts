/* eslint-disable no-console */
import FastestValidator, { ValidationError } from 'fastest-validator'

import DefaultModel from '../models/DefaultModel'

const fastestValidator = new FastestValidator()

const defaultSchemas = {
  id: {
    id: { type: 'uuid' },
  },
  list: {
    page: {
      type: 'number',
      convert: true,
      optional: true,
    },
    itemsPerPage: {
      type: 'number',
      convert: true,
      optional: true,
    },
    sortBy: {
      type: 'object',
      optional: true,
    },
    search: {
      type: 'string',
      empty: true,
      optional: true,
    },
  },
  name: {
    name: {
      type: 'string',
    },
  },
}

const defaultValidators = {
  id: fastestValidator.compile(defaultSchemas.id),
  list: fastestValidator.compile(defaultSchemas.list),
  name: fastestValidator.compile(defaultSchemas.name),
}

export default class DefaultController<T> {
  model: DefaultModel<T>

  validators: {
    list: (value: unknown) => true | ValidationError[],
    create: (value: unknown) => true | ValidationError[],
    update: (value: unknown) => true | ValidationError[],
  }

  constructor(
    model: DefaultModel<T>,
    validators?: {
      list?: (value: unknown) => true | ValidationError[],
      create?: (value: unknown) => true | ValidationError[],
      update?: (value: unknown) => true | ValidationError[],
    },
  ) {
    this.model = model

    this.validators = {
      list: (validators && validators.list) ? validators.list : defaultValidators.list,
      create: (validators && validators.create) ? validators.create : defaultValidators.name,
      update: (validators && validators.update) ? validators.update : defaultValidators.name,
    }
  }

  async list(params: unknown): Promise<T[]> {
    if (params == null) {
      params = {}
    }
    const validation = this.validators.list(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    return this.model.list(params)
  }

  async get(params: unknown): Promise<T> {
    if (params == null) {
      params = {}
    }
    const validation = defaultValidators.id(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    const args = params as {
      id: string,
    }

    return this.model.get(
      args.id,
    )
  }

  async create(params: unknown): Promise<T> {
    if (params == null) {
      params = {}
    }
    const validation = this.validators.create(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    const item = params as T

    return this.model.create(
      item,
    )
  }

  async update(params: unknown): Promise<void> {
    if (params == null) {
      params = {}
    }
    let validation = defaultValidators.id(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    const { id } = params as {id: string}

    validation = this.validators.update(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    const item = params as T

    return this.model.update(
      id,
      item,
    )
  }

  async remove(params: unknown): Promise<void> {
    if (params == null) {
      params = {}
    }
    const validation = defaultValidators.id(params)
    if (validation !== true) {
      console.error(validation)
      throw new Error('validation failed')
    }
    const { id } = params as {id: string}

    return this.model.remove(
      id,
    )
  }
}
