import path from 'path'

// eslint-disable-next-line import/no-extraneous-dependencies
import { app, protocol, BrowserWindow } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'

import { connect as connectDatabase } from '../database'

let win: BrowserWindow | null

protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    autoHideMenuBar: true,
    title: 'Gestion des clients',
    webPreferences: {
      nodeIntegration: false,
      nodeIntegrationInWorker: false,
      nodeIntegrationInSubFrames: false,
      enableRemoteModule: false,
      sandbox: true,
      safeDialogs: true,
      webSecurity: true,
      webviewTag: false,
      nativeWindowOpen: false,
      allowRunningInsecureContent: false,
      contextIsolation: false,
      worldSafeExecuteJavaScript: true,
      preload: path.join(__dirname, 'preload.js'),
    },
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    void win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
  } else {
    createProtocol('app')
    void win.loadURL('app://./index.html')
  }

  win.on('page-title-updated', (evt) => {
    evt.preventDefault()
  })

  win.on('closed', () => {
    win = null
  })
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.on('ready', async () => {
  try {
    await connectDatabase()
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e)
    process.exit()
  }

  await import('./routes/Clients')

  createWindow()
})
