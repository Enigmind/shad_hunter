
import Controller from '../controllers/Clients'

import generateDefaultRouter from './generateDefaultRouter'

export default generateDefaultRouter('clients', Controller)
