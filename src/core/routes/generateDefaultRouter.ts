// eslint-disable-next-line import/no-extraneous-dependencies
import { ipcMain } from 'electron'

import DefaultController from '../controllers/DefaultController'

export default function generateDefaultRouter<T>(
  name: string,
  controller: DefaultController<T>,
): void {
  ipcMain.handle(`${name}-list`, async (event, params: unknown) => controller.list(params))
  ipcMain.handle(`${name}-get`, async (event, params: unknown) => controller.get(params))
  ipcMain.handle(`${name}-create`, async (event, params: unknown) => controller.create(params))
  ipcMain.handle(`${name}-update`, async (event, params: unknown) => controller.update(params))
  ipcMain.handle(`${name}-remove`, async (event, params: unknown) => controller.remove(params))
}

