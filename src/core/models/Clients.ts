import Client from '../../entities/Client'

import DefaultModel from './DefaultModel'

export default new DefaultModel<Client>('Clients')
