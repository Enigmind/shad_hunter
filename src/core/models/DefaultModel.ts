
import { Repository } from 'typeorm'

import { getConnection as database } from '../../database'

export default class DefaultModel <T> {
  repositoryName: string

  repository: Repository<T>

  constructor(repositoryName: string) {
    this.repositoryName = repositoryName
    this.repository = database().getRepository<T>(repositoryName)
  }

  async list(params: unknown): Promise<T[]> {
    return this.repository.find()
  }

  async get(id: string): Promise<T> {
    return this.repository.findOneOrFail(id)
  }

  async create(item: T): Promise<T> {
    return this.repository.save(item)
  }

  async update(id: string, itemUpdated: T): Promise<void> {
    let item = await this.repository.findOneOrFail(id)
    item = { ...itemUpdated }
    await this.repository.save(item)
  }

  async remove(id: string): Promise<void> {
    const item = await this.repository.findOneOrFail(id)
    await this.repository.remove(item)
  }
}
