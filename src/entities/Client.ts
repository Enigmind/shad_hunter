// eslint-disable-next-line import/no-unused-modules
export default interface Client {
  id?: string,
  firstName: string,
  lastName: string,
}
