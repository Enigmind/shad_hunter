import moment from 'moment'

moment.locale('fr')

export function convertUTCToLocal(date: string) :string {
  return moment.utc(date).local().format('DD/MM/YYYY HH:mm')
}
export function convertUTCToLocalDate(date: string) :string {
  return moment.utc(date).local().format('DD/MM/YYYY')
}
export function convertUTCToLocalMonth(date: string) :string {
  return moment.utc(date).local().format('MMMM')
}

