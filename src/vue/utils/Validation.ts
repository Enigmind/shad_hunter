/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

export default {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  generic: [(v: any): boolean | string => !!v || 'Ce champ est requis'],
  // eslint-disable-next-line no-restricted-globals
  number: [(v: any): boolean | string => !isNaN(v) || 'Ce champ doit être rempli par un nombre'],
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  array: [(v: any): boolean | string => v.length > 0 || 'Ce champ est requis'],
}
