import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

const Clients = () => import('../pages/clients/index.vue')
const ClientsList = () => import('../pages/clients/list.vue')
const ClientsCreate = () => import('../pages/clients/create.vue')
const ClientsDetails = () => import('../pages/clients/details.vue')
const ClientsUpdate = () => import('../pages/clients/update.vue')

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/clients',
  },
  {
    path: '/clients',
    component: Clients,
    children: [
      {
        path: '/',
        component: ClientsList,
      },
      {
        path: 'create',
        component: ClientsCreate,
      },
      {
        path: ':id',
        component: ClientsDetails,
      },
      {
        path: ':id/update',
        component: ClientsUpdate,
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
})

export default router
